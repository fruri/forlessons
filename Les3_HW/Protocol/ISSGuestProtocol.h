//
//  ISSGuestProtocol.h
//  Les3_HW
//
//  Created by 16732351 on 09/10/2019.
//  Copyright © 2019 HF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ISSWaiterProtocol.h"

@interface ISSGuest : NSObject <WaiterDelegate>

@property (nonatomic, strong) ISSWaiter *waiter;
@property (nonatomic) int orderedGuestNumber;       //Количество гостей сделавших заказ
@property (nonatomic) int dishCount;              // Число блюд, которое заказал гость

/**
 Гость делает заказ официанту
 */
- (void) MakeOrder;

/**
 Функция считающая удовлетворенность гостя

 @return возвращает доволен он или нет
 */
- (BOOL) SayThanks;

@end

