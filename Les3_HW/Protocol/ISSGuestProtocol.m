//
//  ISSGuestProtocol.m
//  Les3_HW
//
//  Created by 16732351 on 09/10/2019.
//  Copyright © 2019 HF. All rights reserved.
//

#import "ISSGuestProtocol.h"

@implementation ISSGuest

- (instancetype) init
{
    self = [super init];
    if(self)
    {
        self.orderedGuestNumber = 0;
        self.dishCount = 0;
    }
    return self;
}

- (void) MakeOrder
{
    
    NSLog(@"--> Появился новый гость");
    
    self.waiter = [[ISSWaiter alloc] init];
    self.waiter.waiterDelegate = self;
    
    if([self.waiter respondsToSelector:@selector(TakeOrder) ])
    {
        //Официант готов принимать заказ
        self.dishCount = [self.waiter TakeOrder];
    }
    else
    {
        NSLog(@"->Kitchen что-то пошло не так :(");
    }
    
}

- (BOOL) SayThanks
{
    NSLog(@"Что теперь скажет гость?");
    
    //Получаем оценку от клиента, зависит от настроения
    int moodOfGuest = arc4random_uniform(10);
    NSLog(@"Настроение гостя - %d", moodOfGuest);
    
    if (moodOfGuest > 5)
    {
        NSLog(@"=> Было вкусно, спасибо повару!\n");
        return 1;
        
    } else {
        NSLog(@"=> Обойдетесь без чаевых в этот раз\n");
        return 0;
    }
}

@end
