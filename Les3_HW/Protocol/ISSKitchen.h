//
//  ISSKitchen.h
//  Les3_HW
//
//  Created by 16732351 on 09/10/2019.
//  Copyright © 2019 HF. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KitchenDelegate


@end

@interface ISSKitchen : NSObject

@property (nonatomic, weak) id<KitchenDelegate> kitchenDelegate;


/**
 Кухня готовит блюда из заказа

 @param dishCount возвращает сколько блюд готово
 */
- (void) PrepareOrder: (int) dishCount;

@end
