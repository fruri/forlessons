//
//  ISSKitchen.m
//  Les3_HW
//
//  Created by 16732351 on 09/10/2019.
//  Copyright © 2019 HF. All rights reserved.
//

#import "ISSKitchen.h"

static const NSInteger maxForDish = 1000;

@implementation ISSKitchen

- (instancetype)init
{
    self = [super init];
    return self;
}


- (void) PrepareOrder: (int) dishCount
{
    //официант передает заказ на кухню
    
    NSLog(@"На кухне начали готовить!");
   
    //время приготовления, чтобы видеть что что-то готовиться, но не все-все-все цифры
    for (int j=0; j <  dishCount; j++)
     {
         for (int i = 1; i < maxForDish; i++)
         {
             if (i < 4)
             {
                 NSLog(@"%d", i);
             }
             else
             {
                if (i > maxForDish-2)
                {
                    //динамика приготовления блюд
                    NSLog(@"Блюдо №%d готово.", j+1);
                }
     
             }
         }
     }
    
    //Готовка завершена, заказ передается официанту
    NSLog(@"Все блюда готовы!");
};

@end
