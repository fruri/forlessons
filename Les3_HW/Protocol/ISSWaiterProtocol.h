//
//  ISSWaiterProtocol.h
//  Les3_HW
//
//  Created by 16732351 on 10/10/2019.
//  Copyright © 2019 HF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ISSKitchen.h"

@protocol WaiterDelegate



@end

@interface ISSWaiter : NSObject  <KitchenDelegate>

@property (nonatomic, strong)  ISSKitchen *kitchen;
@property (nonatomic, weak) id<WaiterDelegate> waiterDelegate; //Guest

@property (nonatomic) int dishCount;

/**
 Официант фиксрует заказ от гостя, после чего передает его Кухне

 @return возвращает количество заказанных блюд
 */
- (int)TakeOrder;

@end
