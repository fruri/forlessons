//
//  ISSWaiterProtocol.m
//  Les3_HW
//
//  Created by 16732351 on 10/10/2019.
//  Copyright © 2019 HF. All rights reserved.
//

#import "ISSWaiterProtocol.h"

@implementation ISSWaiter

- (instancetype)init
{
    self = [super init];
    if(self)
    {
        self.dishCount = 0;
    }
    return self;
}

//Официант фиксрует заказ от гостя, после чего передает его Кухне
- (int) TakeOrder
{
    //Случайным образом определяем сколько блюд заказал гость
    self.dishCount = arc4random_uniform(4);
    NSLog(@"Гость сделал заказ из %d блюд",self.dishCount);
    
    self.kitchen = [[ISSKitchen alloc] init];
    self.kitchen.kitchenDelegate = self;
    
    if([self.kitchen respondsToSelector:@selector(PrepareOrder:)])
    {
        if(self.dishCount != 0)
        {
            //заказ передан на кухню
            [self.kitchen PrepareOrder:self.dishCount];
            
            //Заказ готов, официант забирает
            NSLog(@"Официант забирает заказ из кухни");
            
            return self.dishCount;
        }
        else
        {
            //Обработка на случай, если гость ничего не заказал
            NSLog(@"Кажется гость ничего не заказал, ждем следующего...\n");
            return self.dishCount = 0;
        }
    }
    else
    {
        NSLog(@"->Kitchen что-то пошло не так :(");
        return self.dishCount = 0;
    }
}

@end
