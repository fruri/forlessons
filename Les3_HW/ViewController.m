//
//  ViewController.m
//  Les3_HW
//
//  Created by 16732351 on 01/10/2019.
//  Copyright © 2019 HF. All rights reserved.
//

#import "ViewController.h"
#import "ISSGuestProtocol.h"
#import "ISSWaiterProtocol.h"
#import "ISSKitchen.h"

@interface ViewController ()

@property (nonatomic, strong) ISSGuest *guest;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    int happyGuestsCount = 0;    //Количество довольных гостей
    int allDishsCount = 0;      //Общее количество приготовленных кухней блюд
    
    self.guest = [[ISSGuest alloc] init];
    
    //Чтобы было не скучно, пусть гостей будет хотя бы 10
    for (int i=0; i<10; i++)
    {
        //Гость делает заказ, дальше все под капотом
        [self.guest MakeOrder];
        
        //Считаем сколько всего бюло гостей, зачем-то храним это в переменной из класса-делегата
        self.guest.orderedGuestNumber ++;
        
        //Если гость заказал что-то, то запускаем заказ в производство
        if(self.guest.dishCount)
        {
        
            allDishsCount += self.guest.dishCount;
            
            //Получаем оценку от клиента, в зависимости от настроения
            if([self.guest SayThanks])
            {
                //Считаем довольных гостей
                happyGuestsCount++;
            }
        }
        
    }
    
    NSLog(@"Сегодня было %d гостей, из них ушли довольными %d. И слопали они %d блюд!", self.guest.orderedGuestNumber, happyGuestsCount, allDishsCount);
}

@end
