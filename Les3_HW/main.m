//
//  main.m
//  Les3_HW
//
//  Created by 16732351 on 01/10/2019.
//  Copyright © 2019 HF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
